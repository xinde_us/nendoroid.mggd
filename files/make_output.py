#!/usr/bin/env python

from __future__ import print_function
# python2 because gimp

import json
import os
import re
import shutil
import struct
import tempfile

# mafiagg_common

def is_png(data):
    return (data[:8] == '\211PNG\r\n\032\n'and (data[12:16] == 'IHDR'))


def get_png_dimensions(file_name):  # Tuple[int, int]
    with open(file_name, 'rb') as f:
        data = f.read(25)  # Header info only
    if is_png(data):
        w, h = struct.unpack('>LL', data[16:24])
        width = int(w)
        height = int(h)
    else:
        raise Exception('not a png image')
    return width, height


def get_image_file_names(dir_path=None, extension=None):  # Generator[str]
    if extension is None:
        extension = '.xcf'
    if not dir_path:
        dir_path = "esports"
    return (f for f in os.listdir(dir_path) if f.endswith(extension))


def gimp_get_images(dir_path=None):  # Generator[GimpImage]
    import gimpfu
    from gimpfu import pdb
    return (pdb.gimp_xcf_load(0, os.path.join(dir_path, xcf_name), xcf_name)
            for xcf_name in get_image_file_names(dir_name, extension='.xcf'))


class MggCharacter:

    def __init__(self, name, bg_color, image_file_path=None):
        self.name = name
        self.bg_color = bg_color
        if image_file_path:
            self.set_image(image_file_path)

    @property
    def id(self):  # int
        return int(self.image_file_name[4:])

    def set_image(self, image_file_path):
        self.width, self.height = get_png_dimensions(image_file_path)
        self.image_file_name = image_file_path.rpartition(os.path.sep)[-1]
        if self.image_file_name.endswith('.png'):
            raise Exception('incorrect format: ends with .png')
        elif not self.image_file_name.startswith('img-'):
            raise Exception('incorrect format: does not start with `img-`')

    def to_mggd_def(self):  # Dict[str, Any]
        return {
            "avatarCrop": {
                "height": self.height,
                "width": self.width,
                "x": 0,
                "y": 0,
            },
            "backgroundColor": self.bg_color,
            "id": self.id,
            "name": self.name,
        }


def color_percent_to_hex(color): # str
    color *= 256
    color = '%02x' % color
    color = color if color != '100' else 'ff'
    return color


def rgb_to_hex(red, green, blue):  # str
    red = color_percent_to_hex(red)
    green = color_percent_to_hex(green)
    blue = color_percent_to_hex(blue)
    return '#%s%s%s' % (red, green, blue)


def gimp_mgg_format_dir(dir_path, deck_name, version, output_file_path, rename_func=None, test=False):
    import gimpfu
    from gimpfu import pdb
    characters = []
    output_data = {
        'builtin': False,
        'characters': characters,
        'name': deck_name,
        'version': version,
    }
    tmp_dir = tempfile.mkdtemp()
    try:
        # Copy character files (and renaming if necessary)
        for i, xcf_name in enumerate(get_image_file_names(dir_path, extension='.xcf')):
            print(xcf_name)
            image = pdb.gimp_xcf_load(0, os.path.join(dir_path, xcf_name), xcf_name)
            layers = image.layers
            content_layer = layers[0]
            bg_layer = layers[1]
            # Get background color
            bg_color_obj = pdb.gimp_image_pick_color(image, bg_layer, 1, 1, gimpfu.FALSE, gimpfu.FALSE, 0)
            bg_color = rgb_to_hex(bg_color_obj.red, bg_color_obj.green, bg_color_obj.blue)
            # Export file for packaging
            pdb.gimp_item_set_visible(bg_layer, gimpfu.FALSE)
            pdb.gimp_image_merge_visible_layers(image, gimpfu.CLIP_TO_IMAGE)
            export_file_name =  'img-%(i)d' % {'i': i}
            export_file_path = os.path.join(tmp_dir, export_file_name)
            drawable = pdb.gimp_image_get_active_drawable(image)
            # pdb.gimp_file_save(image, drawable, os.path.join(dir, file_name), file_name)  # Saves by extension
            pdb.file_png_save_defaults(image, drawable, export_file_path, export_file_name)
            pdb.gimp_image_delete(image)
            if rename_func:
                name = rename_func(xcf_name)
            if name is None:
                continue
            character = MggCharacter(name, bg_color, image_file_path=export_file_path)
            characters.append(character.to_mggd_def())
            if test:
                break
        # Write metadata file
        with open(os.path.join(tmp_dir, 'metadata.json'), 'w') as f:
            f.write(json.dumps(output_data))
        # Zip all the contents together
        shutil.make_archive(output_file_path, 'zip', tmp_dir)  # Appends `.zip`
        shutil.copy(output_file_path + '.zip', output_file_path)
    except Exception:
        shutil.rmtree(tmp_dir)
        raise


def gimp_export_all(base_dir):
    import gimpfu
    from gimpfu import pdb
    errors = 0
    for xcf_name in get_image_file_names(os.path.join(base_dir, 'staging'), extension='.xcf'):
        print(xcf_name)
        image = pdb.gimp_xcf_load(0, os.path.join(base_dir, 'staging', xcf_name), xcf_name)
        # Export file for packaging
        pdb.gimp_image_merge_visible_layers(image, gimpfu.CLIP_TO_IMAGE)
        export_file_name =  xcf_name.replace('xcf', 'png')
        export_file_path = os.path.join(base_dir, 'input', export_file_name)
        drawable = pdb.gimp_image_get_active_drawable(image)
        # pdb.gimp_file_save(image, drawable, os.path.join(dir, file_name), file_name)  # Saves by extension
        if image.width != image.height:
            print('WARNING: %(export_file_name)s is not square' % {'export_file_name': export_file_name})
            errors += 1
        pdb.file_png_save_defaults(image, drawable, export_file_path, export_file_name)
        pdb.gimp_image_delete(image)
    if errors:
        raise Exception('FAIL: Found %(errors) errors' % {'errors': errors})


EXISTING = set()
NAME_MAP = None

name_parts_re = re.compile(r'\[([^\]]*)\] (([^ ]*).*)\.(png|PNG|xcf|XCF)')


def load_name_map(file_name):
    global NAME_MAP
    global EXISTING
    EXISTING = set()
    with open(file_name, 'r') as f:
        NAME_MAP = json.load(f)


def rename(name):  # Optional[str]
    global NAME_MAP
    match = name_parts_re.match(name)
    if not match:
        raise Exception('No regex match: %(name)s' % {'name': name})
    groups = match.groups()
    number = groups[0]
    full_name = groups[1]
    first_name = groups[2]

    new_name = None
    if number in NAME_MAP:
        mapped = NAME_MAP[number]
        if mapped == '*':
            new_name = full_name
        elif mapped == '-':
            return None
        else:
            new_name = mapped
    else:
        new_name = first_name
    return '%(new_name)s.png' % {'new_name': new_name}

def gimp_rename(name):  # Optional[str]
    global EXISTING
    new_name = rename(name)
    if new_name:
        new_name = new_name.rpartition('.')[0]
    else:
        return None
    if new_name in EXISTING:
        raise Exception('Name already exists: %(new_name)s' % {'new_name': new_name})
    EXISTING.add(new_name)
    return new_name


def process_one(in_dir, out_dir, name):
    global EXISTING
    src = os.path.join(in_dir, name)
    new_name = rename(name)
    if not new_name:
        return
    if new_name in EXISTING:
        raise Exception('Name already exists: %(new_name)s' % {'new_name': new_name})
    EXISTING.add(new_name)
    dst = os.path.join(out_dir, new_name)
    shutil.copyfile(src, dst)


def main():
    this_dir = os.path.dirname(__file__)
    base_dir = os.path.join(this_dir, '..')
    in_dir = os.path.join(base_dir, 'input')
    out_dir = os.path.join(base_dir, 'output')

    name_map_file = os.path.join(this_dir, 'name_maps.json')
    load_name_map(name_map_file)

    fnames = os.listdir(in_dir)
    for name in fnames:
        process_one(in_dir, out_dir, name)

def main_gimp_package(deck_name='Nendoroid', version=4, test=False):
    this_dir = os.getcwd()
    base_dir = os.path.join(this_dir, '..')
    in_dir = os.path.join(base_dir, 'input')
    out_dir = os.path.join(base_dir, 'output')
    staging_dir = os.path.join(base_dir, 'staging')

    name_map_file = os.path.join(this_dir, 'name_maps.json')
    load_name_map(name_map_file)

    output_file_path = os.path.join(base_dir, '%(deck_name)s.mggd' % {'deck_name': deck_name})
    gimp_mgg_format_dir(staging_dir, deck_name, version, output_file_path, rename_func=gimp_rename, test=test)

def main_gimp_export():
    this_dir = os.getcwd()
    base_dir = os.path.join(this_dir, '..')
    gimp_export_all(base_dir)

if __name__ == '__main__':
    main()
