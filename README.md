source: https://www.goodsmile.info/en/nendoroid000-100

tools:
* https://www.remove.bg/upload
* https://www.gimp.org/downloads/

steps
1. Remove bg from Nendoroid image and save as `[<id>] <name>.png`.
1. Export it as a gimp project to `./staging/`.
1. Add a bg color and crop to a square and export to `./input` as a `.png`.
1. `cd ./files`
1. `python make_output.py`
1. Package `./output` into an `.mggd`.

helpers
* `make_output.py` contains a couple useful `main` methods that can be used in
  a GIMP python console:
  * Packaging all `.xcf` files as a `.mggd`.
  * Exporting all `.xcf` files to `.png`s.

example: `[0001] Neko Arc.xcf` has gone through full lifecycle.

TODO update to have both programmer-centric and layman-centric instructions
TODO update to leverage gimp scripts